use emergence::AoC;

mod solutions;

type Solver = fn(&str) -> (Option<String>, Option<String>);

pub fn run(aoc: AoC) {
    let problems: Vec<Solver> = vec![
        solutions::template::solve,
        solutions::day1::solve,
        solutions::day2::solve,
    ];

    for (day, solver) in problems.iter().skip(1).enumerate() {
        let day = day + 1;

        let input = aoc.read_or_fetch(day).expect("could not fetch input");

        let (part1, part2) = solver(&input);
        if part1.is_some() {
            let solution = part1.unwrap();
            println!("day {day} part 1 solution:\n{solution}\n");
        }
        if part2.is_some() {
            let solution = part2.unwrap();
            println!("day {day} part 2 solution:\n{solution}\n");
        }
    }
}
