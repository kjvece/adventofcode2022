pub fn solve(input: &str) -> (Option<String>, Option<String>) {
    (part1(input), part2(input))
}

fn part1(input: &str) -> Option<String> {

    fn shape_score(c: &str) -> u32 {
        match c {
            "X" => 1,
            "Y" => 2,
            "Z" => 3,
            _ => 0,
        }
    }

    fn match_score(opponent: &str, own: &str) -> u32 {
        if opponent == "A" {
            match own {
                "X" => { return 3; }
                "Y" => { return 6; }
                "Z" => { return 0; }
                _ => { return 0; }
            }
        }
        if opponent == "B" {
            match own {
                "Y" => { return 3; }
                "Z" => { return 6; }
                "X" => { return 0; }
                _ => { return 0; }
            }
        }
        if opponent == "C" {
            match own {
                "Z" => { return 3; }
                "X" => { return 6; }
                "Y" => { return 0; }
                _ => { return 0; }
            }
        }
        0
    }

    fn score(round: &Vec<&str>) -> u32 {
        let opponent = round.get(0).unwrap_or(&"");
        let own = round.get(1).unwrap_or(&"");
        
        shape_score(own) + match_score(opponent, own)
    }

    let lines: Vec<&str> = input.split("\n").collect();

    let sum: u32 = lines.iter().map(|l| l.split_whitespace().collect::<Vec<&str>>()).map(|l| score(&l)).sum();
    Some(sum.to_string())
}

fn part2(input: &str) -> Option<String> {

    fn match_score(own: &str) -> u32 {
        match own {
            "X" => 0,
            "Y" => 3,
            "Z" => 6,
            _ => 0,
        }
    }

    fn shape_score(opponent: &str, own: &str) -> u32 {
        if opponent == "A" {
            match own {
                "X" => { return 3; }
                "Y" => { return 1; }
                "Z" => { return 2; }
                _ => { return 0; }
            }
        }
        if opponent == "B" {
            match own {
                "X" => { return 1; }
                "Y" => { return 2; }
                "Z" => { return 3; }
                _ => { return 0; }
            }
        }
        if opponent == "C" {
            match own {
                "X" => { return 2; }
                "Y" => { return 3; }
                "Z" => { return 1; }
                _ => { return 0; }
            }
        }
        0
    }

    fn score(round: &Vec<&str>) -> u32 {
        let opponent = round.get(0).unwrap_or(&"");
        let own = round.get(1).unwrap_or(&"");
        
        shape_score(opponent, own) + match_score(own)
    }

    let lines: Vec<&str> = input.split("\n").collect();

    let sum: u32 = lines.iter().map(|l| l.split_whitespace().collect::<Vec<&str>>()).map(|l| score(&l)).sum();
    Some(sum.to_string())
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_part1() {
        let result = part1(&INPUT).unwrap_or_default();
        assert_eq!(result, "15".to_owned(), "got incorrect result: {result}");
    }

    #[test]
    fn test_part2() {
        let result = part2(&INPUT).unwrap_or_default();
        assert_eq!(result, "12".to_owned(), "got incorrect result: {result}");
    }
    
    const INPUT: &str = "A Y
B X
C Z";

}
