pub fn solve(input: &str) -> (Option<String>, Option<String>) {
    (part1(input), part2(input))
}

fn part1(_input: &str) -> Option<String> {
    None
}

fn part2(_input: &str) -> Option<String> {
    None
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_part1() {
        let result = part1(&INPUT).unwrap_or_default();
        assert_eq!(result, "".to_owned(), "got incorrect result: {result}");
    }

    #[test]
    fn test_part2() {
        let result = part2(&INPUT).unwrap_or_default();
        assert_eq!(result, "".to_owned(), "got incorrect result: {result}");
    }
    
    const INPUT: &str = "testinput";

}
