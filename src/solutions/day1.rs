pub fn solve(input: &str) -> (Option<String>, Option<String>) {
    (part1(input), part2(input))
}

fn part1(input: &str) -> Option<String> {

    let lines: Vec<&str> = input.split("\n").collect();

    let mut calories: Vec<u32> = lines
        .split(|x| x.trim() == "")
        .map(|x| x.iter().map(|y| y.parse::<u32>().unwrap_or_default()))
        .map(|x| x.collect::<Vec<u32>>())
        .map(|x| x.iter().sum())
        .collect();

    // sort in descending order
    calories.sort_by(|a,b| b.partial_cmp(a).unwrap());

    // return the highest value
    let highest: u32 = calories.iter().take(1).sum();
    Some(highest.to_string())
}

fn part2(input: &str) -> Option<String> {
    let lines: Vec<&str> = input.split("\n").collect();

    let mut calories: Vec<u32> = lines
        .split(|x| x.trim() == "")
        .map(|x| x.iter().map(|y| y.parse::<u32>().unwrap_or_default()))
        .map(|x| x.collect::<Vec<u32>>())
        .map(|x| x.iter().sum())
        .collect();

    // sort in descending order
    calories.sort_by(|a,b| b.partial_cmp(a).unwrap());

    // return the sum of the highest 3 values
    let top_calories: u32 = calories.iter().take(3).sum();
    Some(top_calories.to_string())
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_part1() {
        let result = part1(&INPUT).unwrap_or_default();
        assert_eq!(result, "24000".to_owned(), "got incorrect result: {result}");
    }

    #[test]
    fn test_part2() {
        let result = part2(&INPUT).unwrap_or_default();
        assert_eq!(result, "45000".to_owned(), "got incorrect result: {result}");
    }

    const INPUT : &str = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";
}
