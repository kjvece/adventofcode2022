use emergence::AoC;

fn main() {
    let aoc = AoC::new(2022).expect("could not create AoC instance");

    aoc2022::run(aoc);
}
